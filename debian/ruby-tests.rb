$: << "test"

Encoding.default_external="utf-8" if defined? Encoding

require "./test/util"

Dir["test/test_*.rb"].each { |f| require f }
